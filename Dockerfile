FROM elixir:1.12-alpine AS build

# install build dependencies
RUN apk add --no-cache build-base git python3
RUN apk add --update nodejs npm

# prepare build dir
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ENV MIX_ENV=prod

# install mix dependencies
COPY apps apps
COPY mix.exs mix.lock ./
COPY config config
RUN mix do deps.get, deps.compile

# build assets
COPY apps/sun_moon_web/assets/package.json apps/sun_moon_web/assets/package-lock.json ./apps/sun_moon_web/assets/
RUN npm install npm@latest -g
RUN npm -v
RUN npm --prefix ./apps/sun_moon_web/assets ci --progress=false --no-audit --loglevel=error

RUN npm run --prefix ./apps/sun_moon_web/assets deploy
WORKDIR /app/apps/sun_moon_web/
RUN mix phx.digest
WORKDIR /app

RUN mix release all


FROM alpine:3.14.0 AS app

RUN apk add --update --no-cache  openssl ncurses-libs curl py-pip py3-scipy

RUN pip install skyfield==1.39
RUN pip install pytz==2021.1

WORKDIR /app
RUN chown nobody:nobody /app
USER nobody:nobody

COPY --from=build --chown=nobody:nobody /app/_build/prod/rel/all ./
COPY --from=build --chown=nobody:nobody /app/apps/sun_moon/python/ apps/sun_moon/python
COPY --chown=nobody:nobody apps/sun_moon/ephemerides/de421.bsp apps/sun_moon/ephemerides/de421.bsp

ENV HOME=/app

CMD ["bin/all", "start"]
