defmodule SunMoonWeb.PageController do
  use SunMoonWeb, :controller

  def index(conn, _params) do
    conn
    |> setAssigns()
    |> render("index.html")
  end

  def setAssigns(conn) do
    ip = conn.remote_ip |> :inet.ntoa() |> to_string()
    location = SunMoon.Location.getCoordinates(ip)

    lat = Map.get(location, "latitude")
    long = Map.get(location, "longitude")
    coords = [lat, long]

    time_zone = Map.get(location, "time_zone")
    region_name = Map.get(location, "region_name")
    city = Map.get(location, "city")
    geo_ip = Map.get(location, "ip")

    {rise, set} = SunMoon.Sun.sunriseAndSunset(coords)
    {midday, midnight} = SunMoon.Sun.middayAndMidnight(coords)

    conn
    |> assign(:moon_phase_in_degrees, SunMoon.Moon.phase_in_degrees())
    |> assign(:moon_fraction_illuminated, SunMoon.Moon.fraction_illuminated())
    |> assign(:moon_next_phases, SunMoon.Moon.next_phases())
    |> assign(:sun_midday, midday)
    |> assign(:sun_midnight, midnight)
    |> assign(:sun_sunrise, rise)
    |> assign(:sun_sunset, set)
    |> assign(:ip, ip)
    |> assign(:long, long)
    |> assign(:lat, lat)
    |> assign(:time_zone, time_zone)
    |> assign(:region_name, region_name)
    |> assign(:city, city)
    |> assign(:geo_ip, geo_ip)
  end
end
