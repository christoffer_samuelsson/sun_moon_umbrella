defmodule SunMoonWeb.PageControllerTest do
  use SunMoonWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Sun & Moon"
  end
end
