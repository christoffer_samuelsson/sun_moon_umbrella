#!/usr/bin/env python

from skyfield.framelib import ecliptic_frame
from skyfield import almanac

import skyfield_helper as sh

def phase_in_degrees():
    time = sh.current_time()

    sun = sh.ephemeris['sun']
    moon = sh.ephemeris['moon']
    earth = sh.ephemeris['earth']

    _, sun_longitude, _ = earth.at(time).observe(sun).apparent().frame_latlon(ecliptic_frame)
    _, moon_longitude, _ = earth.at(time).observe(moon).apparent().frame_latlon(ecliptic_frame)
    unrounded_phase = (moon_longitude.degrees - sun_longitude.degrees) % 360.0
    phase = round(float(unrounded_phase), 4)

    return f'{phase}'

def fraction_illuminated():
    time = sh.current_time()

    sun = sh.ephemeris['sun']
    moon = sh.ephemeris['moon']
    earth = sh.ephemeris['earth']

    unrounded_fraction = almanac.fraction_illuminated(sh.ephemeris, "MOON" , time)
    percentage = unrounded_fraction * 100
    illumination = round(float(percentage), 8)

    return f'{illumination}'

def next_phases():
    time = sh.current_time()
    time_next_month = sh.time_next_month()

    times, phases = almanac.find_discrete(time, time_next_month, almanac.moon_phases(sh.ephemeris))

    times_utc = times.utc_strftime('%Y-%m-%d: %H:%M')
    phases_list = phases.tolist()
    result = [*map(select_moon_phase, phases_list)]

    return [times_utc , result]

def select_moon_phase(phase_number):
    if phase_number == 0: return "New Moon"
    if phase_number == 1: return "First Quarter"
    if phase_number == 2: return "Full Moon"
    if phase_number == 3: return "Third Quarter"
