#!/usr/bin/env python

import datetime as dt
from pytz import timezone
from skyfield import almanac
from skyfield.api import N, W, E, wgs84, load

import skyfield_helper as sh

def set_times():
    zone = timezone('UTC')
    now = zone.localize(dt.datetime.utcnow())
    midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
    next_midnight = midnight + dt.timedelta(days=1)

    ts = load.timescale()
    t0 = ts.from_datetime(midnight)
    t1 = ts.from_datetime(next_midnight)

    return t0, t1

def midday_and_midnight(longitude, latitude):
    t0, t1 = set_times()

    location = wgs84.latlon(longitude * N, latitude * E)

    f = almanac.meridian_transits(sh.ephemeris, sh.ephemeris['Sun'], location)
    times, events = almanac.find_discrete(t0, t1, f)

    times_middays = times[events == 1]
    midday = times_middays[0]

    times_midnights = times[events == 0]
    midnight = times_midnights[0]

    time_zone = timezone("Europe/Stockholm")

    midday_string = tstr = str(midday.astimezone(time_zone))[:19]
    midnight_string = tstr = str(midnight.astimezone(time_zone))[:19]

    return midday_string, midnight_string

def sunrise_and_sunset(longitude, latitude):
    t0, t1 = set_times()

    location = wgs84.latlon(longitude * N, latitude * E)
    times, events = almanac.find_discrete(t0, t1, almanac.sunrise_sunset(sh.ephemeris, location))

    times_sunrises = times[events == 1]
    sunrise = times_sunrises[0]

    times_sunsets = times[events == 0]
    sunset = times_sunsets[0]

    time_zone = timezone("Europe/Stockholm")

    sunrise_string = str(sunrise.astimezone(time_zone))[:19]
    sunset_string = str(sunset.astimezone(time_zone))[:19]

    return sunrise_string ,sunset_string
