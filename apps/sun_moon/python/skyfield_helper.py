#!/usr/bin/env python

def preload_ephemeris():
    from skyfield.api import load_file

    global ephemeris
    ephemeris = load_file('apps/sun_moon/ephemerides/de421.bsp')

def current_time():
    from datetime import datetime
    from pytz import timezone
    from skyfield.api import load

    time_scale = load.timescale()
    time_zone = timezone("UTC")

    datetime_utc = datetime.utcnow()
    localized = time_zone.localize(datetime_utc)
    time = time_scale.from_datetime(localized)

    return time

def time_next_month():
    from datetime import timedelta
    from datetime import datetime
    from pytz import timezone
    from skyfield.api import load

    time_scale = load.timescale()
    time_zone = timezone("UTC")

    datetime_utc = datetime.utcnow()
    next_month_time = datetime_utc + timedelta(days=32)

    localized = time_zone.localize(next_month_time)
    time = time_scale.from_datetime(localized)

    return time
