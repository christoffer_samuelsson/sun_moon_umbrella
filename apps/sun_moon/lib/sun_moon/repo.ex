defmodule SunMoon.Repo do
  use Ecto.Repo,
    otp_app: :sun_moon,
    adapter: Ecto.Adapters.Postgres
end
