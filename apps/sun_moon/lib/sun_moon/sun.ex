defmodule SunMoon.Sun do
  @moduledoc """
  Collects information about the sun.
  """

  def solar_noon(location) do
    SunMoon.PythonServer.call_function(:sun, :solar_noon, location)
  end

  def true_midnight(location) do
    SunMoon.PythonServer.call_function(:sun, :true_midnight, location)
  end

  def sunrise(location) do
    SunMoon.PythonServer.call_function(:sun, :sunrise, location)
  end

  def sunset(location) do
    SunMoon.PythonServer.call_function(:sun, :sunset, location)
  end

  def sunriseAndSunset(location) do
    SunMoon.PythonServer.call_function(:sun, :sunrise_and_sunset, location)
  end

  def middayAndMidnight(location) do
    SunMoon.PythonServer.call_function(:sun, :midday_and_midnight, location)
  end
end
