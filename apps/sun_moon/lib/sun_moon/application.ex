defmodule SunMoon.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      SunMoon.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: SunMoon.PubSub},
      # Start a worker by calling: SunMoon.Worker.start_link(arg)
      # {SunMoon.Worker, arg}
      {SunMoon.PythonServer, []}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: SunMoon.Supervisor)
  end
end
