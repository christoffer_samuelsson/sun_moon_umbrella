defmodule SunMoon.PythonServer do
  use GenServer
  use Export.Python

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    {ok, pid} = Python.start(python_path: Path.expand("apps/sun_moon/python"), python: "python3")
    :python.call(pid, :skyfield_helper, :preload_ephemeris, [])
    {ok, pid}
  end

  def call_function(module, function, args) do
    GenServer.call(__MODULE__, {:call_function, module, function, args})
  end

  def handle_call({:call_function, module, function, args}, _from, pid) do
    result = call_instance(pid, module, function, args)
    {:reply, result, pid}
  end

  def call_instance(pid, module, function, args \\ []) do
    :python.call(pid, module, function, args)
  end
end
