defmodule SunMoon.Moon do
  @moduledoc """
  Collects information about the moon.
  """

  def phase_in_degrees() do
    SunMoon.PythonServer.call_function(:moon, :phase_in_degrees, [])
  end

  def fraction_illuminated() do
    SunMoon.PythonServer.call_function(:moon, :fraction_illuminated, [])
  end

  def next_phases() do
    SunMoon.PythonServer.call_function(:moon, :next_phases, [])
    |> Enum.zip()
  end
end
