defmodule(SunMoon.Location) do
  @moduledoc """
  Geolocation.
  """

  def getCoordinates("127.0.0.1") do
    getCoordinates("80.68.117.0")
  end

  def getCoordinates(ip) do
    ip
    |> buildCoordinatesUrl()
    |> HTTPoison.get!()
    |> Map.get(:body)
    |> Jason.decode!()
  end

  def buildCoordinatesUrl(ip) do
    "https://freegeoip.app/json/#{ip}"
  end
end
